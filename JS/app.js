let firstName = prompt("Enter your name"); // запрашиваем имя пользователя
let lasttName = prompt("Enter your lastname"); // запрашиваем фамилию пользователя
let birthday = prompt('Enter your age (Example: dd.mm.yyyy)') // запрашиваем дату рождения пользователя

function createNewUser(firstName, lastName, birthday) {  // создаем функцию по имени и фамилии
    let newUser = {
        name: firstName,
        lastname: lastName,
        birthday,
            getLogin() {
            return `${firstName.charAt(0).toLowerCase()}${lastName.toLowerCase()}`  // берем первую букву имени и фамилию, делаем все прописными, соединяем через шаблонные кавычки.
        },

            getAge() {

        const userBirthdayYear = this.birthday.slice(6);  // отделяем год рождения от остальной строки
        const userBirthdayMonth = this.birthday.slice(3, 5) - 1; // отделяем месяц рождения от остальной строки
        const userBirthdayDay = this.birthday.slice(0, 2); // отделяем день рождения от остальной строки

        const now = new Date(); //Текущя дата
        const today = new Date(now.getFullYear(), now.getMonth(), now.getDate()); //Текущя дата без времени
        const userDateBirth = new Date(userBirthdayYear, userBirthdayMonth, userBirthdayDay); //Дата рождения с использованием переменных, полученных из ответа пользователя. 
        const userDateBirthThisYear = new Date(today.getFullYear(), userDateBirth.getMonth(), userDateBirth.getDate()); //ДР в текущем году


        let age = today.getFullYear() - userDateBirth.getFullYear(); // если ДР в этом году уже был.

            if (today < userDateBirthThisYear) { //Если ДР в этом году ещё не был, то убираем 1 год из age
                age = age-1;
        }
   
        return age
    
        },

            getPassword() {
                return `${firstName.charAt(0).toUpperCase()}${lastName.toLowerCase()}${birthday.slice(6)}` // выбираем первую букву имени и делаем ее в верхнем регистре, берем фамилию полностью в нижнем регистре и применяем слайс к дате рождения (полученной через промпт).
        }


    }
    return newUser;  // возвращаем результат функции
}

console.log(createNewUser(firstName, lasttName, birthday).getLogin());  // выводим в консоль функцию и метод, определяя логин пользователя. 
console.log(createNewUser(firstName, lasttName, birthday).getAge()); // выводим в консоль функцию и метод, определяя возраст пользователя.
console.log(createNewUser(firstName, lasttName, birthday).getPassword()); // выводим в консоль функцию и метод, определяя пароль пользователя.



